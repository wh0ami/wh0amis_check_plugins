#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys
import xml.parsers.expat

import nagiosplugin
import requests
import xmltodict


# data acquisition
class NextcloudUpdate(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, domain: str, port: int, notls: bool) -> None:
        self.__domain = domain
        self.__port = port
        self.__noTLS = notls

    # main check method
    def probe(self) -> dict:
        # build url
        baseurl = f'{"http://" if self.__noTLS else "https://"}{self.__domain}'
        baseurl += f':{str(self.__port)}' if self.__port > 0 else ''
        baseurl += '/status.php'

        # define request headers
        headers = {'User-Agent': 'check_nextcloud_update/1.0', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache'}

        # fetch the current version that is in use in the nextcloud instance
        resp: requests.Response = requests.get(baseurl, headers=headers, stream=True)

        # parse response values out of version string from nextcloud
        version = resp.json()['version']
        versiondict = version.split('.')

        # fetch data from Nextcloud Updater API and parse the XML data to a dict
        raw = requests.get(
            f'https://updates.nextcloud.com/updater_server/?version={versiondict[0]}x{versiondict[1]}x{versiondict[2]}x{versiondict[3]}xxxstablexxx999x0x0',
            headers=headers, stream=True)
        try:
            apidata: dict = xmltodict.parse(raw.text)
            needs_update = 1
            is_eol = int(apidata['nextcloud']['eol'])
        except xml.parsers.expat.ExpatError:
            needs_update = 0
            is_eol = 0

        # check whether the version is up-to-date and/or EOL
        yield nagiosplugin.Metric(name='is_end_of_life', value=is_eol, context='is_end_of_life')
        yield nagiosplugin.Metric(name='needs_update', value=needs_update, context='needs_update')


# data presentation
class NextcloudUpdateSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return 'Nextcloud instance is up to date!'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made for Nextcloud servers- it fetches the current version from the server and '
                    'checks, whether there is an update available for that server.',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-d', '--domain', metavar='<Nextcloud domain>', type=str, required=True,
                        help='The domain of the web frontend, e.g. cloud.my-domain.org')
    parser.add_argument('-p', '--port', metavar='<port>', type=int, required=False, default=0,
                        help='The port to use. If not passed, the plugin will use the standard port of the current '
                             'protocol.')
    parser.add_argument('-n', '--noTls', action='store_true',
                        help='Pass this parameter, if the plugin should use HTTP instead of HTTPS.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        NextcloudUpdate(domain=args.domain, port=args.port, notls=args.noTls),
        nagiosplugin.ScalarContext(name='is_end_of_life', critical="0"),
        nagiosplugin.ScalarContext(name='needs_update', warning="0"), NextcloudUpdateSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
