#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import os
import platform
import sys

import nagiosplugin


# data acquisition
class Uptime(nagiosplugin.Resource):
    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # get the uptime from /proc/uptime and return the value in seconds
        uptime_file = "/proc/uptime"

        try:
            with open(uptime_file, "r") as handle:
                yield nagiosplugin.Metric(name='uptime_seconds',
                                          value=int(round(float(handle.readline().split()[0]), 0)),
                                          context='uptime_seconds', uom='s')
                handle.close()
        except FileNotFoundError:
            raise nagiosplugin.CheckError(f"{uptime_file} does not exist!")
        except PermissionError:
            raise nagiosplugin.CheckError(f"Access to {uptime_file} was denied!")
        except ValueError:
            raise nagiosplugin.CheckError(f"{uptime_file} does not contain a valid timestamp!")

        # check whether reboot is required (the hidden file occurs on some armbian based systems)
        if os.path.exists("/var/run/reboot-required") or os.path.exists(
                "/var/run/.reboot-required") or platform.release() not in os.listdir("/lib/modules/"):
            reboot_required = 1
        else:
            reboot_required = 0
        yield nagiosplugin.Metric(name='reboot_required', value=reboot_required, context='reboot_required')


# data presentation
class UptimeSummary(nagiosplugin.Summary):
    def __generateMessage(self, results: dict) -> str:
        # transform the uptime seconds into a readable format
        days, seconds = divmod(results['uptime_seconds'].metric.value, 86400)
        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)

        # generate message
        output = f'{days} days, {hours} hours, {minutes} minutes and {seconds} seconds'
        output += ' - reboot is required!' if bool(results['reboot_required'].metric.value) else ''

        # return the output message
        return output

    def ok(self, results: dict) -> str:
        return self.__generateMessage(results)

    def problem(self, results: dict) -> str:
        return self.__generateMessage(results)


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor the reboot status and the uptime of Linux systems',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-w', '--uptime-warning', metavar='<range in seconds>', type=str, required=False,
                        help='The warning range for the uptime in seconds metric. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('-c', '--uptime-critical', metavar='<range in seconds>', type=str, required=False,
                        help='The critical range for the uptime in seconds metric. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('-n', '--no-reboot-warning', action='store_true',
                        help='Pass this parameter, if you do not want to be warned if a reboot is required.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Uptime(),
                               nagiosplugin.ScalarContext(name='uptime_seconds', warning=args.uptime_warning,
                                                          critical=args.uptime_critical),
                               nagiosplugin.ScalarContext(name='reboot_required',
                                                          warning=None if args.no_reboot_warning else '0'),
                               UptimeSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
