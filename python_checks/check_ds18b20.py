#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import os
import sys

import nagiosplugin


# data acquisition
class Ds18b20(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, identifier: str, fahrenheit: bool, kelvin: bool) -> None:
        self.__identifier = identifier
        self.__fahrenheit = fahrenheit
        self.__kelvin = kelvin

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # initialize device path
        devicepath = f"/sys/bus/w1/devices/{self.__identifier}/w1_slave"

        # check whether -k/--kelvin and -f/--fahrenheit were passed together
        if self.__fahrenheit and self.__kelvin:
            raise nagiosplugin.CheckError("-k/--kelvin and -f/--fahrenheit can't be used together. Aborting!")

        # check whether the sensor is connected and serves data
        if not os.path.exists(devicepath):
            raise nagiosplugin.CheckError(f"{devicepath} does not exist - is the sensor correctly connected?")

        # read the sensor data
        f = open(devicepath, "r")
        sensoroutput = f.readlines()
        f.close()

        # check whether the CRC is invalid
        yield nagiosplugin.Metric(name="crcinvalid", value=1 if sensoroutput[0].split(" ")[-1].strip() != "YES" else 0,
                                  context="crcinvalid")

        # read the temperature value and yield it (comes by default in °C, will be converted if requested)
        temperature: float = float(sensoroutput[1].split(" ")[-1].split("=")[-1])

        if self.__fahrenheit:
            temperature = temperature * 1.8 + 32000
            uom = "F"
        elif self.__kelvin:
            temperature = temperature + 273150
            uom = "K"
        else:
            uom = "C"

        yield nagiosplugin.Metric(name="temperature", value=round(temperature / 1000, 1), context="temperature",
                                  uom=uom)


# data presentation
class Ds18b20Summary(nagiosplugin.Summary):
    # read passed parameters
    def __init__(self, identifier: str, fahrenheit: bool, kelvin: bool) -> None:
        self.__identifier = identifier
        self.__fahrenheit = fahrenheit
        self.__kelvin = kelvin

    def ok(self, results: dict) -> str:
        degree = "°" if results['temperature'].metric.uom != "K" else ""
        return f"Sensor {self.__identifier} is reporting {results['temperature'].metric.value}{degree}{results['temperature'].metric.uom}, CRC is valid"


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor the temperature reported by a DS18B20 sensor via 1-Wire',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-i', '--identifier', metavar='<identifier of the DS18B20 sensor>', type=str, required=True,
                        help='The identifier of the DS18B20 sensor that should be used. You can find this ID by using '
                             '`ls /sys/bus/w1/devices/`.')
    parser.add_argument('-w', '--warning', metavar='<value in degrees>', type=str, required=False,
                        help='The warning range for the temperature in degrees. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('-c', '--critical', metavar='<value in degrees>', type=str, required=False,
                        help='The critical range for the temperature in degrees. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('-f', '--fahrenheit', action='store_true',
                        help='Pass this parameter, if you want to get the result in °F instead of °C. Incompatible '
                             'with -k/--kelvin.')
    parser.add_argument('-k', '--kelvin', action='store_true',
                        help='Pass this parameter, if you want to get the result in Kelvin instead of °C. '
                             'Incompatible with -f/--fahrenheit.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Ds18b20(identifier=args.identifier, fahrenheit=args.fahrenheit, kelvin=args.kelvin),
                               nagiosplugin.ScalarContext(name='temperature', warning=args.warning,
                                                          critical=args.critical),
                               nagiosplugin.ScalarContext(name='crcinvalid', warning=1),
                               Ds18b20Summary(identifier=args.identifier, fahrenheit=args.fahrenheit,
                                              kelvin=args.kelvin))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
