#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys
from collections import namedtuple
from typing import Union

import nagiosplugin
import psutil


# data acquisition
class Cpu(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, detailed: bool, interval: int) -> None:
        self.__detailed = detailed
        self.__interval = interval

    # method for generating nagiosplugin metrics from psutil objects
    def __getMetrics(self, data: namedtuple, prefix: str) -> nagiosplugin.Metric:
        # iterate over all attributes from the object, that are not protected/private and that are not callable
        for attr in [a for a in dir(data) if not a.startswith('_') and not callable(getattr(data, a))]:
            # guest value is part of user value and guest_nice value is part of nice value - so we subtract
            # the values here, since we otherwise see overall sums greater than 100% that doesn't make any sense
            if attr == "user":
                value = round(getattr(data, attr) - getattr(data, "guest"), 1)
            elif attr == "guest_nice":
                value = round(getattr(data, attr) - getattr(data, "guest_nice"), 1)
            else:
                value = getattr(data, attr)
            yield nagiosplugin.Metric(name=f'{prefix}{attr}', value=value, context=attr, uom='%')
        yield nagiosplugin.Metric(name=f'{prefix}usage', value=round(100.0 - data.idle, 2), context='usage', uom='%')

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # information about cpu will be directly fetched
        yield nagiosplugin.Metric(name='cores', value=int(psutil.cpu_count(logical=False)), context='other')
        yield nagiosplugin.Metric(name='threads', value=int(psutil.cpu_count(logical=True)), context='other')

        # fetch cpu metrics
        cpu: Union[list, namedtuple] = psutil.cpu_times_percent(interval=self.__interval, percpu=self.__detailed)

        # based on the data type of the command above, yield the data
        results = []
        if isinstance(cpu, list):
            for threadindex in range(len(cpu)):
                thread: namedtuple = cpu[threadindex]
                results.extend(self.__getMetrics(data=thread, prefix=f'cpu{threadindex}_'))
        else:
            results.extend(self.__getMetrics(data=cpu, prefix=f''))
        for result in results:
            yield result


# data presentation
class CpuSummary(nagiosplugin.Summary):
    # read passed parameters
    def __init__(self, detailed: bool) -> None:
        self.__detailed = detailed

    def ok(self, results: dict) -> str:
        if self.__detailed:
            msg = 'cpu usage overview:'
            for result in results:
                if result.metric.name.endswith('_usage'):
                    msg += f'\n[{result.metric.name.split("_")[0]}] {result.metric.value}%'
        else:
            msg = f'Overall cpu usage is {results["usage"].metric.value}%'
        return msg


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # define the metrics
    metrics = ['user', 'nice', 'system', 'idle', 'iowait', 'irq', 'softirq', 'steal', 'guest', 'guest_nice', 'usage']

    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check to monitor the cpu usage on a Linux system',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-i', '--interval', metavar='<amount of seconds>', type=int, required=False, default=5,
                        help='The amount of seconds to sleep between fetching the cpu data, defaults to 5 seconds.')
    parser.add_argument('-d', '--detailed', action='store_true',
                        help='Whether the percentages should be fetched per cpu thread.')
    for metric in metrics:
        parser.add_argument(f'--warning-{metric}', metavar='<range in percent>', type=str, required=False, default=None,
                            help=f'The warning range for the cpu {metric} metric. Don\'t pass, if you don\'t want to '
                                 'be warned.')
        parser.add_argument(f'--critical-{metric}', metavar='<range in percent>', type=str, required=False,
                            default=None,
                            help=f'The critical range for the cpu {metric} metric. Don\'t pass, if you don\'t want to '
                                 'be warned.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        Cpu(interval=args.interval, detailed=args.detailed),
        nagiosplugin.ScalarContext(name='other'),
        CpuSummary(detailed=args.detailed)
    )

    # add cpu context objects to check object
    for metric in metrics:
        check.add(nagiosplugin.ScalarContext(name=metric, warning=getattr(args, f'warning_{metric}'),
                                             critical=getattr(args, f'critical_{metric}')))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
