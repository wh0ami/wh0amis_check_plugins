#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import json.decoder
import statistics
import sys
import time
from typing import Union

from haversine import haversine, Unit
import nagiosplugin
import requests


# class for the distance-groups
class DistanceGroup:
    # initialize the counter attribute of the object (amount of airplanes in the distance group)
    __counter = 0

    # read passed parameters
    def __init__(self, minimum_distance: int, maximum_distance: int) -> None:
        self.__min = minimum_distance
        self.__max = maximum_distance

    # method for inreasing the counter attribute
    def increaseCounter(self) -> None:
        self.__counter += 1

    # getter for the attributes
    def getCounter(self) -> int:
        return self.__counter

    def getMinimumDistance(self) -> int:
        return self.__min

    def getMaximumDistance(self) -> int:
        return self.__max


# data acquisition
class Adsb(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, domain: str, port: int, notls: bool, interval: int, idle_threshold: int,
                 path_prefix: str) -> None:
        self.__domain = domain
        self.__port = port
        self.__noTLS = notls
        self.__interval = interval
        self.__idle_threshold = idle_threshold
        self.__path_prefix = path_prefix

    # method for fetching ADS-B data from the web frontend
    def __getData(self) -> Union[dict, None]:
        # build base url
        baseurl = f'{"http://" if self.__noTLS else "https://"}{self.__domain}'
        baseurl += f':{str(self.__port)}' if self.__port > 0 else ''
        baseurl += self.__path_prefix

        # define request headers
        headers = {'User-Agent': 'check_adsb_rates/1.0', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache'}

        # fetch the receiver information
        receiver_data: requests.Response = requests.get(url=baseurl + f'data/receiver.json?{str(time.time())}',
                                                        headers=headers)

        # fetch the ads-b data
        adsb_data: requests.Response = requests.get(url=baseurl + f'data/aircraft.json?{str(time.time())}',
                                                    headers=headers)

        # return the data
        try:
            return {'adsb': adsb_data.json(), 'receiver': receiver_data.json()}
        except json.decoder.JSONDecodeError:
            return None

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # crawl data
        data1 = self.__getData()
        time.sleep(self.__interval)
        data2 = self.__getData()
        if not data1 or not data2:
            raise nagiosplugin.CheckError("Received invalid JSON from the tar1090 API!")

        # parse, calculate and yield the messages per second
        yield nagiosplugin.Metric(name='messages_per_second', value=int(
            round(data2['adsb']['messages'] - data1['adsb']['messages']) / (
                    data2['adsb']['now'] - data1['adsb']['now'])), context='messages_per_second')

        # parse the distances and initialize variables
        distance_list = []
        aircraft_counter = 0
        maximum_distance = 0.0
        distance_groups = {'0-79': DistanceGroup(0, 79), '80-159': DistanceGroup(80, 159),
                           '160-239': DistanceGroup(160, 239), '240-319': DistanceGroup(240, 319),
                           '320-399': DistanceGroup(320, 399), '400+': DistanceGroup(400, 99999999999)}

        # iterate over list of all aircraft
        for aircraft in data2['adsb']['aircraft']:
            # check, whether the datapoint is not older than requested via --idle-threshold
            if aircraft['seen'] < self.__idle_threshold:
                # increase the counter
                aircraft_counter += 1

                # check whether the data provides information about the position of the aircraft
                if 'lat' in aircraft and 'lon' in aircraft:
                    # calculate distance to the aircraft in kilometers
                    distance: float = haversine((data2['receiver']['lat'], data2['receiver']['lon']),
                                                (aircraft['lat'], aircraft['lon']), unit=Unit.KILOMETERS)

                    # append aircraft to the distance-group counters
                    for distance_group in distance_groups.keys():
                        if distance_groups[distance_group].getMinimumDistance() <= distance <= distance_groups[
                            distance_group].getMaximumDistance():
                            distance_groups[distance_group].increaseCounter()
                            break

                    # append aircraft to the distance list
                    distance_list.append(distance)

                    # append aircraft to the max distance counter, if distance is the currently highest
                    maximum_distance = distance if distance > maximum_distance else maximum_distance

        # parse and yield the amount of aircraft
        yield nagiosplugin.Metric(name='aircraft', value=aircraft_counter, context='aircraft')

        # yield aircraft with maximum distance
        yield nagiosplugin.Metric(name='maximum_distance', value=round(maximum_distance, 2), context='other', uom='km')

        # yield average distance
        yield nagiosplugin.Metric(name='average_distance', value=round(statistics.mean(distance_list), 2),
                                  context='other', uom='km')

        # yield distance-group values
        for distance_group in distance_groups.keys():
            yield nagiosplugin.Metric(name=f'distance-group_{distance_group}',
                                      value=distance_groups[distance_group].getCounter(), context='other')


# data presentation
class AdsbSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return f'Feeder is reporting {str(results["messages_per_second"].metric.value)} messages per second and an ' \
               f'amount of {str(results["aircraft"].metric.value)} aircraft'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made for the tar1090 web frontend and partially its forks like e.g. PiAware '
                    'SkyAware - it fetches the same json file as the frontend itself and parses it',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-d', '--domain', metavar='<web frontend domain>', type=str, required=True,
                        help='The domain of the web frontend, e.g. ten90.my-domain.org')
    parser.add_argument('-p', '--port', metavar='<port>', type=int, required=False, default=0,
                        help='The port to use. If not passed, the plugin will use the standard port of the current '
                             'protocol.')
    parser.add_argument('-n', '--noTls', action='store_true',
                        help='Pass this parameter, if the plugin should use HTTP instead of HTTPS.')
    parser.add_argument('-i', '--interval', metavar='<amount of seconds>', type=int, required=False, default=5,
                        help='The amount of seconds to sleep between fetching the messages per second, defaults to 5 '
                             'seconds.')
    parser.add_argument('--messages-warning', metavar='<amount of messages per second>', type=str, required=False,
                        default='', help='The warning range for the messages per second metric. Don\'t pass, if you '
                                         'don\'t want to be warned.')
    parser.add_argument('--messages-critical', metavar='<amount of messages per second>', type=str, required=False,
                        default='', help='The critical range for the messages per second metric. Don\'t pass, if you '
                                         'don\'t want to be warned.')
    parser.add_argument('--aircraft-warning', metavar='<amount of aircraft>', type=str, required=False, default='',
                        help='The warning range for the aircraft amount metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--aircraft-critical', metavar='<amount of aircraft>', type=str, required=False, default='',
                        help='The critical range for the aircraft amount metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--idle-threshold', metavar='<amount of seconds>', type=int, required=False, default=60,
                        help='Airplanes with a datapoint age, which is higher than this threshold, will be ignored. '
                             'Defaults to 60 seconds.')
    parser.add_argument('--path-prefix', metavar='<URL prefix (string)>', type=str, required=False, default='/tar1090/',
                        help='The prefix, that should be added to paths like e.g. "data/aircraft.json". Defaults to '
                             '"/tar1090/" - can be set to "/" for e.g. PiAware SkyAware.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Adsb(domain=args.domain, port=args.port, notls=args.noTls, interval=args.interval,
                                    idle_threshold=args.idle_threshold, path_prefix=args.path_prefix),
                               nagiosplugin.ScalarContext(name='messages_per_second', warning=args.messages_warning,
                                                          critical=args.messages_critical),
                               nagiosplugin.ScalarContext(name='aircraft', warning=args.aircraft_warning,
                                                          critical=args.aircraft_critical),
                               nagiosplugin.ScalarContext(name='other'), AdsbSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
