#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys
import time

import nagiosplugin
import requests


# data acquisition
class Jitsi(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, domain: str, port: int, notls: bool) -> None:
        self.__domain = domain
        self.__port = port
        self.__noTLS = notls

    # method for fetching usage stats from the colibri api of jitsi
    def __getData(self) -> dict:
        # build url
        baseurl = 'http://' if self.__noTLS else 'https://'
        baseurl += self.__domain
        baseurl += f':{str(self.__port)}' if self.__port > 0 else ''

        # define request headers
        headers = {
            'User-Agent': 'check_jitsi_usage/1.0',
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache'
        }

        # fetch the information from the colibri stats endpoint
        response: requests.Response = requests.get(url=baseurl + f'/colibri/stats?{str(time.time())}', headers=headers)

        # return the information
        return {'status': response.status_code, 'json': response.json()}

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # crawl data
        data = self.__getData()

        # yield the amount of video conferences
        yield nagiosplugin.Metric(name='conferences', value=data["json"]["conferences"], context='conferences')

        # yield the amount of participants
        yield nagiosplugin.Metric(name='participants', value=data["json"]["participants"], context='participants')

        # yield the other generic performance data
        for key, val in data["json"].items():
            if key not in ["conferences", "participants"]:
                try:
                    yield nagiosplugin.Metric(name=key, value=float(val), context='other')
                except ValueError:
                    pass
                except TypeError:
                    pass


# data presentation
class JitsiSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return (f'Jitsi instance is reporting {str(results["conferences"].metric.value)} conferences ' +
                f'and an amount of {str(results["participants"].metric.value)} participants')


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made for Jitsi Meet - it fetches the colibri stats api and parses its output.',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-d', '--domain', metavar='<web frontend domain>', type=str, required=True,
                        help='The domain of the API, e.g. localhost')
    parser.add_argument('-p', '--port', metavar='<port>', type=int, required=False, default=0,
                        help='The port to use. If not passed, the plugin will use the standard port of the current '
                             'protocol.')
    parser.add_argument('-n', '--noTls', action='store_true',
                        help='Pass this parameter, if the plugin should use HTTP instead of HTTPS.')
    parser.add_argument('--conferences-warning', metavar='<amount of conferences>', type=str, required=False,
                        help='The warning range for the conference amount metric. Don\'t pass, if you '
                             'don\'t want to be warned.')
    parser.add_argument('--conferences-critical', metavar='<amount of conferences>', type=str, required=False,
                        help='The critical range for the conference amount metric. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('--participants-warning', metavar='<amount of participants>', type=str, required=False,
                        help='The warning range for the participant amount metric. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('--participants-critical', metavar='<amount of participants>', type=str, required=False,
                        help='The critical range for the participant amount metric. Don\'t pass, if you don\'t want to '
                             'be warned.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        Jitsi(domain=args.domain, port=args.port, notls=args.noTls),
        nagiosplugin.ScalarContext(name='conferences', warning=args.conferences_warning,
                                   critical=args.conferences_critical),
        nagiosplugin.ScalarContext(name='participants', warning=args.participants_warning,
                                   critical=args.participants_critical),
        nagiosplugin.ScalarContext(name='other'),
        JitsiSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
