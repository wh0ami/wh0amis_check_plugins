#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin
import os
import pkg_resources
import re
import subprocess
import sys
from typing import Union


# data acquisition
class PiHoleUpdates(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, components: list) -> None:
        self.__components = components

    # method for executing the pihole version check script
    def __executeCheckScript(self, componentParameter: str, fetchLatest: bool) -> str:
        # set path to pihole version check script
        checkscript = "/opt/pihole/version.sh"

        # check whether the check script exists and is executable, if not the check will abort
        if not os.path.exists(checkscript):
            raise nagiosplugin.CheckError(f"PiHole version check script {checkscript} doesn't exist!")
        elif not os.access(checkscript, os.X_OK):
            raise nagiosplugin.CheckError(f"PiHole version check script {checkscript} is not executable!")

        # decide whether to fetch latest or current version
        versionParameter = "-l" if fetchLatest else "-c"

        # execute the version check script and store its output in
        proc: Union[subprocess.Popen[bytes], subprocess.Popen] = subprocess.Popen(
            [checkscript, componentParameter, versionParameter], stdout=subprocess.PIPE)
        proc.wait()
        output = []
        for rawline in proc.stdout.readlines():
            output.append(rawline.decode("utf-8").strip())
        exitcode: int = proc.returncode

        # check whether the version check script returned exit code 0, abort if that's not the case
        if exitcode != 0:
            buffer = f"Check script aborted with exit code {exitcode}\n\nOutput:\n"
            for line in output:
                buffer += f"{line}\n"
            raise nagiosplugin.CheckError(buffer)

        # split the first output line and get version from it
        version = output[0].split(" ")[3]

        # check whether version information value is invalid
        if not re.match(r"^v[0-9.]+$", version):
            raise nagiosplugin.CheckError(
                f"Returned version string '{version}' for parameter '{componentParameter}' is invalid!")

        # finally, return the version string
        return version

    def probe(self) -> nagiosplugin.Metric:
        # iterate over the components
        for component in self.__components:
            # fetch the version information from the specific component
            currentVersion = pkg_resources.parse_version(
                self.__executeCheckScript(componentParameter=component[1], fetchLatest=False))
            latestVersion = pkg_resources.parse_version(
                self.__executeCheckScript(componentParameter=component[1], fetchLatest=True))

            # compare the version information and yield the specific result
            yield nagiosplugin.Metric(name=f"{component[0]}_update_required",
                                      value=1 if currentVersion < latestVersion else 0, context="version_checks")


class PiHoleUpdatesSummary(nagiosplugin.Summary):
    def ok(self, results) -> str:
        return "all PiHole components are up to date"


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
    # define the pihole components and its parameters
    components = [["Pi-hole", "-p"], ["AdminLTE", "-a"], ["FTL", "-f"]]

    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor PiHole installations for available updates',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-c', '--critical', action='store_true',
                        help='Whether updates should be shown as critical instead of as warning.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        PiHoleUpdates(components=components),
        nagiosplugin.ScalarContext(name='version_checks', warning="0" if not args.critical else None,
                                   critical="0" if args.critical else None),
        PiHoleUpdatesSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
