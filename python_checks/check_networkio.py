#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import re
import sys
from collections import namedtuple
from time import sleep
from typing import Union

import nagiosplugin
import psutil


# data acquisition
class Network(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, interval: int, exclude: str, summarize: bool, metrics: list) -> None:
        self.__interval = interval
        self.__exclude = exclude
        self.__pernic = False if summarize else True
        self.__metrics = metrics

    # method for getting a list of all NICs
    def __getNics(self) -> dict:
        # fetch data from psutil
        data: dict = psutil.net_if_stats()

        # filter out all unwanted NICs
        result = {}
        for nic in data:
            if not re.match(self.__exclude, nic):
                tmp: namedtuple = data[nic]
                result[nic] = {"metadata": tmp, "performance": None}

        # return the result dict
        return result

    # method for fetching data from the NICs
    def __getUsage(self) -> dict:
        # fetch data from psutil
        data: Union[list, namedtuple] = psutil.net_io_counters(pernic=self.__pernic)

        # fetch all NICs with its metadata
        results = self.__getNics()

        # assign the performance data to the results nic
        if self.__pernic:
            for nic in results:
                results[nic]["performance"] = data[nic]
        else:
            results["summarized"] = {"metadata": None, "performance": data}

        # return the results dict
        return results

    # method for correct preparation of the metric objects
    def __getDiff(self, val1: int, val2: int, byteToMbit: bool) -> float:
        value = (val2 - val1) * 8 / 1024 / 1024 if byteToMbit else val2 - val1
        return round(float(value / self.__interval), 2)

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # fetch the counter data
        stats1 = self.__getUsage()
        sleep(self.__interval)
        stats2 = self.__getUsage()

        # initialize list of links that are down
        linksdown = []

        # iterate over all NICs and metrics and yield the calculated data as metrics
        for nic in stats2.keys():
            # yield the nic metadata if it exists
            if stats2[nic]["metadata"] is not None:
                # increase counter of the link status, if the NIC is down
                linksdown.append(nic) if not stats2[nic]["metadata"].isup else None

                # yield other metadata
                yield nagiosplugin.Metric(name=f'{nic}_isup', value=1 if stats2[nic]["metadata"].isup else 0,
                                          context="other")
                yield nagiosplugin.Metric(name=f'{nic}_linkspeed', value=stats2[nic]["metadata"].speed, context="other")
                yield nagiosplugin.Metric(name=f'{nic}_mtu', value=stats2[nic]["metadata"].mtu, context="other")

            # yield performance data metrics if it exists and link if the link NIC not down
            if stats2[nic]["performance"] is not None and nic not in linksdown:
                for metric in self.__metrics:
                    if metric[0] != "links_down":
                        yield nagiosplugin.Metric(name=f'{nic}_{metric[1]}',
                                                  value=self.__getDiff(getattr(stats1[nic]["performance"], metric[0]),
                                                                       getattr(stats2[nic]["performance"], metric[0]),
                                                                       True if metric[0].startswith(
                                                                           "bytes") else False),
                                                  context=metric[1])

        # yield the counter with the link status and the total amount of network links
        yield nagiosplugin.Metric(name="links_down", value=len(linksdown), context="links_down")
        yield nagiosplugin.Metric(name="links_total", value=len(stats2) if self.__pernic else len(stats2) - 1,
                                  context="other")


# data presentation
class NetworkSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return 'normal workload on all interfaces'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # define the metrics
    metrics = [
        ["errin", "pps_err_rx", "error packets per second"],
        ["errout", "pps_err_tx", "error packets per second"],
        ["dropin", "pps_drop_rx", "dropped packets per second"],
        ["dropout", "pps_drop_tx", "dropped packets per second"],
        ["bytes_recv", "mbps_rx", "mebibit per second"],
        ["bytes_sent", "mbps_tx", "mebibit per second"],
        ["packets_recv", "pps_rx", "packets per second"],
        ["packets_sent", "pps_tx", "packets per second"],
        ["links_down", "links_down", "network interfaces that are down"]
    ]

    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor the network interfaces of Linux systems',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-e', '--exclude', metavar='<regex pattern>', type=str, required=False,
                        default="^(fw|veth|tap|br-|docker|vmbr).+|lo$",
                        help='RegEx Pattern with NICs to hide, defaults to "^(fw|veth|tap|br-|docker|vmbr).+|lo$".')
    parser.add_argument('-i', '--interval', metavar='<amount of seconds>', type=int, required=False, default=5,
                        help='The amount of seconds to sleep between fetching the network interface data, defaults to '
                             '5 seconds.')
    parser.add_argument('-s', '--summarize', action='store_true',
                        help='Whether the plugin should summarize the metrics of all interfaces.\n'
                             'WARNING: This option can cause wrong results, since traffic on e.g. `bond0.102` will '
                             'also pass `bond0`. VLANs, bonding in general, bridges (virtualization and container '
                             'hosts), VPN interfaces and even more scenarios will lead to wrong results with this '
                             'option. Only use it, if you know what you are doing.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')
    for metric in metrics:
        for situation in ["warning", "critical"]:
            parser.add_argument(f'--{situation}-{metric[1].replace("_", "-")}', metavar=f'<range of {metric[2]}>',
                                type=str, required=False, help=f'The {situation} threshold for the amount of '
                                                               f'{metric[2]}. Don\'t pass, if you don\'t want to be '
                                                               f'warned.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        Network(interval=args.interval, exclude=args.exclude, summarize=args.summarize, metrics=metrics),
        nagiosplugin.ScalarContext(name='other'),
        NetworkSummary()
    )

    # add network context objects to check object
    for metric in metrics:
        check.add(nagiosplugin.ScalarContext(name=metric[1], warning=getattr(args, f'warning_{metric[1]}'),
                                             critical=getattr(args, f'critical_{metric[1]}')))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
