#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys
from collections import namedtuple
from typing import Union

import nagiosplugin
import psutil


# data acquisition
class Memory(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, cachesasused: bool) -> None:
        self.__cachesAsUsed = cachesasused

    # function for custom rounding
    def __cRound(self, value: Union[int, float], toMiB: bool) -> float:
        return round(value / 1024 / 1024 if toMiB else value, 2)

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # read memory stats
        mem: namedtuple = psutil.virtual_memory()

        # yield the metric values
        for metric in ["total", "free", "available", "buffers", "used", "cached", "shared"]:
            yield nagiosplugin.Metric(name=metric, value=self.__cRound(getattr(mem, metric), True), context='other',
                                      uom='MB')

        # based on passed parameter, yield the default percentage or calculate an own percentage including caches
        yield nagiosplugin.Metric(name='usage', value=self.__cRound(
            (mem.total - mem.free) / (mem.total / 100) if self.__cachesAsUsed else mem.percent, False), context='usage',
                                  uom='%')
        yield nagiosplugin.Metric(name='used',
                                  value=self.__cRound(mem.total - mem.free if self.__cachesAsUsed else mem.used, True),
                                  context='other', uom='MB')


# data presentation
class MemorySummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return f'{results["usage"].metric.value}% of memory in use'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(description='This is a check to monitor the usage of the memory of a Linux system',
                                     formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-w', '--warning', metavar='<range in percent>', type=str, required=False, default=None,
                        help='The warning range for the memory usage metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('-c', '--critical', metavar='<range in percent>', type=str, required=False, default=None,
                        help='The critical range for the memory usage metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('-u', '--caches-as-used', action='store_true',
                        help='Calculate the used and usage value via total - free (interpret caches etc. as used)')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Memory(cachesasused=args.caches_as_used),
                               nagiosplugin.ScalarContext(name='usage', warning=args.warning, critical=args.critical),
                               nagiosplugin.ScalarContext(name='other'), MemorySummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
