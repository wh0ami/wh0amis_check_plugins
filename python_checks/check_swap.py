#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
from collections import namedtuple

import nagiosplugin
import psutil
import sys
from time import sleep
from typing import Union


# data acquisition
class Swap(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, interval: int) -> None:
        self.__interval = interval

    # function for custom rounding
    def __cRound(self, value: Union[int, float], toMiB: bool = True) -> float:
        return round(value / 1024 / 1024 if toMiB else value, 2)

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # first fetch of swap stats
        stats1: namedtuple = psutil.swap_memory()

        # yield the total swap size metric
        yield nagiosplugin.Metric(name="total", value=self.__cRound(stats1.total), context="total", uom="MB")

        # proceed only, if there is swap installed in the system
        if stats1.total > 0:
            # wait and fetch swap stats second time
            sleep(self.__interval)
            stats2: namedtuple = psutil.swap_memory()

            # yield the metric values
            yield nagiosplugin.Metric(name="used", value=self.__cRound(stats2.used), context="other", uom="MB")
            yield nagiosplugin.Metric(name="usage", value=self.__cRound(stats2.percent, False), context="usage", uom="%")
            yield nagiosplugin.Metric(name="mibs_write", value=self.__cRound(stats2.sin - stats1.sin, True), context="io")
            yield nagiosplugin.Metric(name="mibs_read", value=self.__cRound(stats2.sout - stats1.sout, True),
                                      context="io")


# data presentation
class SwapSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        # return a result message based on the fact, whether there is swap space in the machine
        if results["total"].metric.value > 0:
            return f'{results["usage"].metric.value}% of swap in use'
        else:
            return 'system is running without configured swap space'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(description='This is a check to monitor the usage of the swap of a Linux system',
                                     formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-i', '--interval', metavar='<amount of seconds>', type=int, required=False, default=5,
                        help='The amount of seconds to sleep between fetching the swap stats, defaults to '
                             '5 seconds.')
    parser.add_argument('--warning-usage', metavar='<range in percent>', type=str, required=False, default=None,
                        help='The warning range for the swap usage metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--critical-usage', metavar='<range in percent>', type=str, required=False, default=None,
                        help='The critical range for the swap usage metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--warning-io', metavar='<range in MiB/s>', type=str, required=False, default=None,
                        help='The warning range for the swap I/O metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--critical-io', metavar='<range in MiB/s>', type=str, required=False, default=None,
                        help='The critical range for the swap I/O metric. Don\'t pass, if you don\'t want to be '
                             'warned.')
    parser.add_argument('--warn-no-swap', action='store_true',
                        help='Pass this parameter, if you want to be warned if there is not swap configured.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Swap(interval=args.interval),
                               nagiosplugin.ScalarContext(name='usage', warning=args.warning_usage,
                                                          critical=args.critical_usage),
                               nagiosplugin.ScalarContext(name='io', warning=args.warning_io,
                                                          critical=args.critical_io),
                               nagiosplugin.ScalarContext(name='total', warning="1:" if args.warn_no_swap else None),
                               nagiosplugin.ScalarContext(name='other'), SwapSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
