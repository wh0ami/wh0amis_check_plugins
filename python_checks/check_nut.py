#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys

import nagiosplugin
import PyNUT


# data acquisition
class Nut(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, host: str, port: int, ups: str, metrics: dict) -> None:
        self.__host = host
        self.__port = port
        self.__ups = ups
        self.__metrics = metrics

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # try to create client for NUT server
        try:
            client: PyNUT.PyNUTClient = PyNUT.PyNUTClient(host=self.__host, port=self.__port)
        except PyNUT.PyNUTError:
            raise nagiosplugin.CheckError("Connection to NUT server failed!")

        # try to fetch ups data from the NUT server
        try:
            data: dict = client.GetUPSVars(ups=self.__ups)
        except PyNUT.PyNUTError:
            raise nagiosplugin.CheckError(f"Your NUT server has never heard of a ups called '{self.__ups}' before!")

        # iterate over the dict of data, that was returned
        for metric in data:
            # check if the metric is part of the predefined metrics - if yes, add its value to its context
            # otherwise, the metric will be thrown in the other context, if it's a numeric value
            metric_name_in_dict = metric.decode("utf-8").replace(".", "_")
            if metric_name_in_dict in self.__metrics.keys():
                yield nagiosplugin.Metric(name=metric_name_in_dict, value=float(data[metric]),
                                          context=metric_name_in_dict)
            else:
                try:
                    yield nagiosplugin.Metric(name=metric.decode("utf-8"), value=float(data[metric]), context="other")
                except ValueError:
                    pass


# data presentation
class NutSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return (f'Charge level: {str(results["battery_charge"].metric.value)} % / Load: '
                f'{str(results["ups_load"].metric.value)} %VA')


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # define the metrics
    metrics = {
        "input_voltage": ["volt", "220:240", "215:245"],
        "input_frequency": ["hertz", "49.9:50.1", "49.8:50.2"],
        "output_voltage": ["volt", "220:240", "215:245"],
        "output_frequency": ["hertz", "49.9:50.1", "49.8:50.2"],
        "battery_charge": ["percent", "85:", "75:"],
        "ups_load": ["percent VoltAmpere", 70, 80]
    }

    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made for NUT (Network UPS Tools) - monitoring for your UPS!',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('--host', metavar='<IP or domain of the NUT server>', type=str, required=True,
                        help='The IP or domain, where your NUT server is listening and reachable (mostly upsd.conf).')
    parser.add_argument('-p', '--port', metavar='<port>', type=int, required=True, default=0,
                        help='The port, where your NUT server is listening and reachable (mostly upsd.conf).')
    parser.add_argument('-u', '--ups', metavar='<UPS name from the NUT config>', type=str, required=True, default=0,
                        help='The name of the UPS, that has been set in the NUT config (mostly in ups.conf).')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    for metric in metrics.keys():
        for situation in ["warning", "critical"]:
            parser.add_argument(f'--{situation}-{metric.replace("_", "-")}', metavar=f'<range of {metrics[metric][0]}>',
                                type=str, required=False,
                                default=metrics[metric][1] if situation == "warning" else metrics[metric][2],
                                help=f'The {situation} threshold for the '
                                     f'{metric.replace("_", " ")}. Defaults to \''
                                     f'{metrics[metric][1] if situation == "warning" else metrics[metric][2]}\''
                                     f'. Pass empty value (\'\'), if you don\'t want to be '
                                     f'warned.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        Nut(host=args.host, port=args.port, ups=args.ups, metrics=metrics),
        nagiosplugin.ScalarContext(name='other'),
        NutSummary())

    # add ups/nut context objects to check object
    for metric in metrics:
        check.add(nagiosplugin.ScalarContext(name=metric, warning=getattr(args, f'warning_{metric}'),
                                             critical=getattr(args, f'critical_{metric}')))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
