#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

# basically an unofficial fork of https://github.com/muensmedia/check_hetzner_storagebox
# thanks to muensmedia ^^

import argparse
import sys

import nagiosplugin
import requests


# data acquisition
class Storagebox(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, username: str, password: str, storagebox_id: str) -> None:
        self.__username = username
        self.__password = password
        self.__storagebox_id = storagebox_id

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # build API url for the Hetzner Robot Webservice with the passed Storagebox ID
        api_url = f'https://robot-ws.your-server.de/storagebox/{self.__storagebox_id}'

        # fetch the information about the Storagebox from the Hetzner API
        api_response: requests.Response = requests.get(api_url, auth=(self.__username, self.__password))

        if api_response.status_code == 200:
            # Storagebox information successfully fetched

            # parse the json response into a dict
            data: dict = api_response.json()['storagebox']

            # parse and yield the storagebox type
            yield nagiosplugin.Metric(name='class', value=int(data['product'].split('X')[1]), context='other')

            # calculate the usage in percent
            usage: float = round((data['disk_usage'] / data['disk_quota']) * 100, 2)

            # yield the metric with the information about the Storagebox usage
            yield nagiosplugin.Metric(name=f'usage', value=usage, min=0, max=100, context='usage', uom='%')
            yield nagiosplugin.Metric(name=f'used', value=data['disk_usage'], min=0, max=data["disk_quota"],
                                      context='used', uom='MiB')

        elif api_response.status_code == 404:
            # Storagebox not found (at least with this user)
            raise nagiosplugin.CheckError(
                'The Storagebox couldn\'t be found (API returned HTTP statuscode 404) - please check the passed '
                'Storagebox ID!')
        elif api_response.status_code == 401:
            # invalid credentials
            raise nagiosplugin.CheckError(
                'Permission denied (API returned HTTP statuscode 401) - please check the passed credentials!')
        elif 500 <= api_response.status_code <= 599:
            # internal server error at Hetzner
            raise nagiosplugin.CheckError(
                f'Internal server error (API returned statuscode {api_response.status_code} - try again in a few '
                f'minutes!')
        else:
            # general error message for all other failures
            raise nagiosplugin.CheckError(
                'Unknown error while fetching Storagebox information (API returned statuscode '
                f'{api_response.status_code})')

            # data presentation


class StorageboxSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        used_gb: float = round(results["used"].metric.value / 1024, 2)
        quota_gb: float = round(results["used"].metric.max / 1024, 2)
        return f'{used_gb} GiB of {quota_gb} GiB in use'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check to monitor the usage of a Hetzner Storagebox',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-u', '--username', metavar='<username>', help='Hetzner Robot Webservice username',
                        required=True)
    parser.add_argument('-p', '--password', metavar='<password>', help='Hetzner Robot Webservice password',
                        required=True)
    parser.add_argument('-i', '--identifier', type=int, help='ID of the Hetzner Storagebox', required=True)
    parser.add_argument('-w', '--warning', type=int, required=False,
                        help='The warning range for the usage of the storagebox in percent. Don\'t pass, if you '
                             'don\'t want to be warned.')
    parser.add_argument('-c', '--critical', type=int, required=False,
                        help='The critical range for the usage of the storagebox in percent. Don\'t pass, if you '
                             'don\'t want to be warned.')
    parser.add_argument('-x', '--warning-mib', type=int, required=False,
                        help='The warning range for the usage of the storagebox in mebibyte. Don\'t pass, if you '
                             'don\'t want to be warned.')
    parser.add_argument('-d', '--critical-mib', type=int, required=False,
                        help='The critical range for the usage of the storagebox in mebibyte. Don\'t pass, if you '
                             'don\'t want to be warned.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Storagebox(username=args.username, password=args.password,
                                          storagebox_id=args.identifier),
                               nagiosplugin.ScalarContext(name='usage', warning=args.warning, critical=args.critical),
                               nagiosplugin.ScalarContext(name='used', warning=args.warning_mib,
                                                          critical=args.critical_mib),
                               nagiosplugin.ScalarContext(name='other'),
                               StorageboxSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
