#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import re
import sys
from collections import namedtuple
from time import sleep

import nagiosplugin
import psutil


# data acquisition
class Disk(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, interval: int, exclude: str, summarize: bool, metrics: list) -> None:
        self.__interval = interval
        self.__exclude = exclude
        self.__perdisk = False if summarize else True
        self.__metrics = metrics

    # method for fetching data from the disks
    def __getUsage(self) -> dict:
        # fetch data from psutil
        data: namedtuple = psutil.disk_io_counters(perdisk=self.__perdisk)

        # initialize empty results dict
        results = {}

        # assign the data to the results dict, filter out all unwanted disks
        if self.__perdisk:
            for disk in data:
                if not re.match(self.__exclude, disk):
                    results[disk]: namedtuple = data[disk]
        else:
            results["summarized"]: namedtuple = data

        # return the results dict
        return results

    # method for correct preparation of the metric objects
    def __getDiff(self, val1: int, val2: int, byteToMiB: bool) -> float:
        value = (val2 - val1) / 1024 / 1024 if byteToMiB else val2 - val1
        return round(float(value / self.__interval), 2)

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # fetch the counter data
        stats1 = self.__getUsage()
        sleep(self.__interval)
        stats2 = self.__getUsage()

        # iterate over all disks and metrics and yield the calculated data as metrics
        for disk in stats2.keys():
            for metric in self.__metrics:
                yield nagiosplugin.Metric(name=f'{disk}_{metric[1]}',
                                          value=self.__getDiff(getattr(stats1[disk], metric[0]),
                                                               getattr(stats2[disk], metric[0]),
                                                               True if metric[0].endswith("bytes") else False),
                                          context=metric[1])


# data presentation
class DiskSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return 'normal workload on all disks'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # define the metrics
    metrics = [["read_bytes", "mibs_read", "mebibyte per second"], ["write_bytes", "mibs_write", "mebibyte per second"],
               ["read_count", "iops_read", "read operations per second"],
               ["write_count", "iops_write", "write operations per second"]]

    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor the network interfaces of Linux systems',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-e', '--exclude', metavar='<regex pattern>', type=str, required=False,
                        default="^(loop[0-9]+|ram[0-9]+|zd.+|dm-[0-9]+|[hsv]d[a-z][0-9]+|(nvme[0-9]+n|mmcblk)[0-9]+p[0-9]+)$",
                        help='RegEx Pattern with Disks to hide, defaults to '
                             '"^(loop[0-9]+|ram[0-9]+|zd.+|dm-[0-9]+|[hs]d[a-z][0-9]+|(nvme[0-9]+n|mmcblk)[0-9]+p[0-9]+)$".')
    parser.add_argument('-i', '--interval', metavar='<amount of seconds>', type=int, required=False, default=5,
                        help='The amount of seconds to sleep between fetching the disk I/O data, defaults to '
                             '5 seconds.')
    parser.add_argument('-s', '--summarize', action='store_true',
                        help='Whether the plugin should summarize the metrics of all interfaces.\n'
                             'WARNING: This option can cause wrong results, since I/O on a hard drive could e.g. also '
                             'go through one or more RAID device(s). Only use it, if you know what you are doing.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')
    for metric in metrics:
        for situation in ["warning", "critical"]:
            parser.add_argument(f'--{situation}-{metric[1].replace("_", "-")}', metavar=f'<range of {metric[2]}>',
                                type=str, required=False, help=f'The {situation} threshold for the amount of '
                                                               f'{metric[2]}. Don\'t pass, if you don\'t want to be '
                                                               f'warned.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        Disk(interval=args.interval, exclude=args.exclude, summarize=args.summarize, metrics=metrics), DiskSummary())

    # add network context objects to check object
    for metric in metrics:
        check.add(nagiosplugin.ScalarContext(name=metric[1], warning=getattr(args, f'warning_{metric[1]}'),
                                             critical=getattr(args, f'critical_{metric[1]}')))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
