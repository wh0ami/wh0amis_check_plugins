#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys

import nagiosplugin
import psutil


# data acquisition
class Procstatus(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, state: str) -> None:
        self.__state = state

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # validate state parameter
        if self.__state not in ['running', 'sleeping', 'disk-sleep', 'stopped', 'tracing-stop', 'zombie', 'dead',
                                'wake-kill', 'waking', 'idle', 'parked']:
            raise nagiosplugin.CheckError("Invalid state passed!")

        # initialize empty return counter
        counter = 0

        # iterate over all system processes
        for pid in psutil.pids():
            try:
                # fetch the status of the current process
                pstatus: str = psutil.Process(pid).status()

                # increase the counter, if the status matches the given state
                counter += 1 if pstatus == self.__state else 0
            except psutil.NoSuchProcess:
                pass

        # yield the counter and the selected state
        yield nagiosplugin.Metric('amount', counter, context='amount')


# data presentation
class ProcstatusSummary(nagiosplugin.Summary):
    # read passed parameters
    def __init__(self, state: str) -> None:
        self.__state = state

    def ok(self, results: dict) -> str:
        return f'{str(results["amount"].metric.value)} with process state {self.__state}'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check to monitor the amount of processes with a given status on a Linux system',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-s', '--state', metavar='<process state>', type=str, required=True, default=None,
                        help='The process state to watch out for. Can be either running, sleeping, disk-sleep, stopped,'
                             ' tracing-stop, zombie, dead, wake-kill, waking, idle or parked.')
    parser.add_argument('-w', '--warning', metavar='<amount as integer>', type=str, required=False, default=None,
                        help='The warning range for the amount of processes. Don\'t pass, if you don\'t want '
                             'to be warned.')
    parser.add_argument('-c', '--critical', metavar='<amount as integer>', type=str, required=False, default=None,
                        help='The critical range for the amount of processes. Don\'t pass, if you don\'t want '
                             'to be warned.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Procstatus(state=args.state),
                               nagiosplugin.ScalarContext(name='amount', warning=args.warning, critical=args.critical),
                               ProcstatusSummary(state=args.state))

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
