#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys

import nagiosplugin
import psutil


# data acquisition
class Procmem(nagiosplugin.Resource):
    # read passed parameters
    def __init__(self, user: str, procstr: str) -> None:
        self.__user = user
        self.__procstr = procstr

    # main check method
    def probe(self) -> nagiosplugin.Metric:
        # find process that matches the input parameters
        # after that, yield its Resident Set Size memory consumption
        proc = None
        proclist: iter[psutil.Process] = psutil.process_iter([])
        for proc in proclist:
            if hasattr(proc, 'info'):
                if proc.info["username"] == self.__user and ' '.join(proc.info["cmdline"]) == self.__procstr:
                    data: psutil.pmem = proc.memory_info()
                    yield nagiosplugin.Metric('proc_mem_bytes', data.rss, context='memory')
                    break

        # raise a check error if there is no existing process
        if 'data' not in locals():
            raise nagiosplugin.CheckError(
                f"There is no process that matches the process string '{self.__procstr}' in the context of the user '{self.__user}'")


# data presentation
class ProcmemSummary(nagiosplugin.Summary):
    def ok(self, results: dict) -> str:
        return f'The process is returning {str(results["proc_mem_bytes"].metric.value)} bytes as Resident Set-Size'


# runtime environment and data evaluation
@nagiosplugin.guarded
def main() -> None:
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check to monitor the memory consumption of a process on a Linux system',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-u', '--user', metavar='<name of the executing user>', type=str, required=True, default=None,
                        help='The executing user of the process that should be searched.')
    parser.add_argument('-p', '--process', metavar='<process string>', type=str, required=True, default=None,
                        help='The FULL string of the process that should be searched (including parameters). Please '
                             'note that the plugin will return in each case the first (oldest) process, if there are '
                             'multiple matches.')
    parser.add_argument('-w', '--warning', metavar='<amount of bytes>', type=str, required=False, default=None,
                        help='The warning range for the memory consumption of the process. Don\'t pass, if you don\'t '
                             'want to be warned.')
    parser.add_argument('-c', '--critical', metavar='<amount of bytes>', type=str, required=False, default=None,
                        help='The critical range for the memory consumption of the process. Don\'t pass, if you don\'t '
                             'want to be warned.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(Procmem(user=args.user, procstr=args.process),
                               nagiosplugin.ScalarContext(name='memory', warning=args.warning, critical=args.critical),
                               ProcmemSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
