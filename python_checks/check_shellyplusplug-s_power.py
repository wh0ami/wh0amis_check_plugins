#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import sys

import nagiosplugin
import requests
from requests.auth import HTTPDigestAuth


# data acquisition
class ShellyPlusPlugSPower(nagiosplugin.Resource):
    def __init__(self, ip: str, user: str, password: str) -> None:
        self.__ip = ip
        # prepare authentication
        if len(user) > 0 and len(password) > 0:
            self.__auth = HTTPDigestAuth(user, password)
        else:
            self.__auth = None

    def probe(self) -> nagiosplugin.Metric:
        # build url
        url = f'http://{self.__ip}/rpc/Switch.GetStatus?id=0'

        # define request headers
        headers = {'User-Agent': 'check_shellyplusplug-s/1.0', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache'}

        # fetch the status from the shellyplusplug-s
        resp: requests.Response = requests.get(url, headers=headers, stream=True, auth=self.__auth)

        # validate the http response code
        if resp.status_code == 200:
            # parse response values out of status json
            power = resp.json()['apower']

            # return the metric
            yield nagiosplugin.Metric(name='power_usage',
                                      value=power, context="power_check")
        else:
            raise nagiosplugin.CheckError(
                f"ShellyPlusPlug S returned invalid HTTP response code '{resp.status_code}' with message '{resp.text}'")


class ShellyPlusPlugSSummary(nagiosplugin.Summary):
    def ok(self, results) -> str:
        return f"ShellyPlusPlug-S reported {results['power_usage'].metric.value}W throughput"


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
    # initialize cli argument parser
    parser = argparse.ArgumentParser(
        description='This is a check made to monitor the power throughput of ShellyPlusPlug S devices',
        formatter_class=argparse.RawTextHelpFormatter)

    # read cli parameters
    parser.add_argument('-i', '--ip', metavar='<IP address of the device>', type=str, required=True,
                        help='The IP address of the device, e.g. 192.168.33.1')
    parser.add_argument('-u', '--user', metavar='<username for authentication>', type=str, required=False, default="",
                        help='The username for authentication. Don\'t pass this parameter, if authentication is '
                             'turned off.')
    parser.add_argument('-p', '--password', metavar='<password for authentication>', type=str, required=False,
                        default="",
                        help='The password for authentication. Don\'t pass this parameter, if authentication is '
                             'turned off.')
    parser.add_argument('-w', '--warning', metavar='<power in watt>', type=int, required=False, default=1500,
                        help='The warning threshold in watt for the power throughput, defaults to 1500.')
    parser.add_argument('-c', '--critical', metavar='<power in watt>', type=int, required=False, default=1750,
                        help='The critical threshold in watt for the power throughput, defaults to 1750.')
    parser.add_argument('-t', '--timeout', metavar='<amount of seconds>', type=int, required=False, default=30,
                        help='Abort execution after the passed amount of seconds, defaults to 30.')

    # actually parse the arguments that were passed
    args = parser.parse_args(sys.argv[1:])

    # define the check
    check = nagiosplugin.Check(
        ShellyPlusPlugSPower(ip=args.ip, user=args.user, password=args.password),
        nagiosplugin.ScalarContext(name='power_check', warning=args.warning, critical=args.critical),
        ShellyPlusPlugSSummary())

    # run the check
    check.main(timeout=args.timeout)


# run the main class
if __name__ == '__main__':
    main()
