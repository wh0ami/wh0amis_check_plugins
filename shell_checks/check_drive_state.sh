#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

hdparmpath=$(which hdparm)

if [ -z "$hdparmpath" ]; then
  printf "Please install hdparm first!\n"
  exit 3
fi

drives=$*
if [ -z "$drives" ] || [ "$1" = "-h" ]; then
  printf "Usage: %s <space separated list of disk ids>\n\nPossible disk IDs are:\n" "$0"
  # shellcheck disable=SC2010 # nice idea, but globs doesn't support regex in the way we need it here
  ls -l /dev/disk/by-id/ | grep -E "sd[a-z]$"
  exit 3
fi

output="HARD DRIVE STATES |"

for drive in $drives; do
  if [ ! -e "/dev/disk/by-id/${drive}" ]; then
    printf "A drive with the id '%s' doesn't exist in this system!\n" "${drive}"
    exit 3
  fi
  state=$(sudo "$hdparmpath" -C "/dev/disk/by-id/${drive}" | awk '/drive state is/ {print $4;}')
  [ "$state" = "standby" ] && val=0 || val=1
  output="${output} ${drive}=${val}"
done

printf "%s\n" "$output"

exit 0
