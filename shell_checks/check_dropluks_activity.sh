#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

dockerpath="$(which docker)"
sudopath="$(which sudo)"

if [ -z "$dockerpath" ] || [ -z "$sudopath" ]; then
  echo "Please install docker and sudo first!"
  exit 3
fi

if [ -z "$1" ] || [ "$1" = "-h" ]; then
  printf "Usage: %s <docker container name or id>\nPossible docker containers can be listed with \`docker ps\`\n\n" "$0"
  printf "Please also make sure, that the executing user is allowed to run \`sudo docker logs <docker container name or id>\`\n"
  exit 3
fi

buffer="/var/tmp/dropluks_last_value_$1.txt"

[ -e "$buffer" ] && last=$(cat "$buffer") || last=0

value=$("$sudopath" "$dockerpath" logs "$1" | grep -c -E " wurde entsperrt!")

[ "$value" -ne "$last" ] && echo "$value" >"$buffer"
value=$((value - last))
[ "$value" -lt 0 ] && value=0

printf "%s successful decryptions since last measurement | decryptions=%s\n" "$value" "$value"

exit 0
