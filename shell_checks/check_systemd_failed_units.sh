#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

systemctlpath=$(which systemctl)
jqpath=$(which jq)

if [ -z "$systemctlpath" ] || [ -z "$jqpath" ]; then
  printf "Please install systemd and jq first!\n"
  exit 3
fi

failed_unit_count=$("$systemctlpath" list-units --failed --output=json | "$jqpath" '. | length')
warning_threshold=1
critical_threshold=2

if [ "$failed_unit_count" -le 0 ]; then
  status="OK"
  exitcode=0
elif [ "$failed_unit_count" -ge $critical_threshold ]; then
  status="CRITICAL"
  exitcode=2
elif [ "$failed_unit_count" -ge $warning_threshold ]; then
  status="WARNING"
  exitcode=1
else
  status="UNKNOWN"
  exitcode=3
fi

printf "SYSTEMD %s - %s units are on state \"failed\"" "$status" "$failed_unit_count"
printf "| failed_unit_count=%s;%s;%s\n" "$failed_unit_count" "$warning_threshold" "$critical_threshold"
exit $exitcode
