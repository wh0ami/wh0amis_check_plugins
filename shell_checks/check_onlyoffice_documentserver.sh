#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

response() {
  printf "ONLYOFFICE DOCUMENTSERVER STATUS "

  case "$2" in
    "0")
      printf "OK"
      ;;
    "1")
      printf "WARNING"
      ;;
    "2")
      printf "CRITICAL"
      ;;
    *)
      printf "UNKNOWN"
      ;;
  esac

  printf ": %s | " "$1"

  if [ $2 = "0" ]; then
    printf "healthcheck_true=1"
  else
    printf "request_success=0"
  fi

  printf "\n"

  exit "$2"
}

CURL_PATH=$(which curl)

if [ -z "$CURL_PATH" ]; then
  response "curl is not installed!" 3
fi
if [ -z "$1" ] || [ -z "$2" ]; then
  response "USAGE: ./check_onlyoffice_documentserver.sh <hostname> <remote path>" 3
fi

HEALTH="$("$CURL_PATH" --fail --silent -A "check_onlyoffice_documentserver/1.0" https://${1}${2})"
CURL_EXIT_CODE=$?

if [ "$CURL_EXIT_CODE" -ne 0 ]; then
  response "Request failed, curl returned exit code '$CURL_EXIT_CODE'!" 2
fi

if [ "$HEALTH" = "true" ]; then
  STATUS_CODE=0
elif [ "$HEALTH" = "false" ]; then
  STATUS_CODE=2
else
  STATUS_CODE=1
fi
response "Healthcheck returned '${HEALTH}'" $STATUS_CODE
