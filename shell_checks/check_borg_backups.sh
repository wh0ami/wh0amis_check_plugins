#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

# basically a unofficial fork of https://github.com/bebehei/nagios-plugin-check_borg/blob/master/check_borg
# thanks bebehei :P

# read parameters
while getopts "b:c:w:h" opt; do
  case "$opt" in
  b)
    basedir="$OPTARG"
    ;;
  c)
    critical="$OPTARG"
    criticalDelta=$(($(date +'%s') - OPTARG * 60 * 60))
    ;;
  w)
    warning="$OPTARG"
    warningDelta=$(($(date +'%s') - OPTARG * 60 * 60))
    ;;
  *)
    printf "Parameters:\n  -b <directory>\t\t- base directory with ONLY repository directories inside\n  -c <duration in hours, int>\t- age in hours of the last backup, after which the check should go on critical\n  -w <duration in hours, int>\t- age in hours of the last backup, after which the check should go on warning\n  -h\t\t\t\t- shows this message\n"
    exit 0
    ;;
  esac
done

if [ -z "$basedir" ] || [ -z "$critical" ] || [ -z "$warning" ]; then
  printf "UNKNOWN - Missing parameter, pass -h for help\n"
  exit 3
else
  cd "$basedir" || exit 1
  export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
  export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
  printf "Results of borg repository check:\n"
  ec=0

  for repo in *; do
    # check whether directory is a borg repository
    if [ -f "${repo}/config" ] && [ -d "${repo}/data" ]; then
      # fetch timestamp of last backup (will be empty if encrypted)
      lastBackup=$(borg list --sort timestamp --last 1 --format '{time}' "$repo" 2>/dev/null)

      if [ -n "$(find "${repo}/lock.exclusive" -mmin +720 2>/dev/null)" ]; then
        # critical, if the repositories lockfile is older than 12 hours
        printf "[CRITICAL] repo %s was locked for more than 12 hours\n" "$repo"
        ec=2
      elif [ -f "${repo}/lock.exclusive" ]; then
        # otherwise, if the lockfile of the repository is younger, we state okay - that's the case, if a backup is currently running
        printf "[OK] repo %s is currently locked - maybe theres a backup running?\n" "$repo"
      elif [ -n "${lastBackup}" ]; then
        # as stated above, lastBackup will be empty if the repository is encrypted, so we check whether the variable is not empty

        # calculate timedelta from now to point of last backup in seconds
        lastBackupSec=$(date -d "$lastBackup" '+%s')

        # compare the calculated value to the values, that were passed by parameter to the script
        if [ "$lastBackupSec" -lt "$criticalDelta" ]; then
          key="CRITICAL"
          ec=2
        elif [ "$lastBackupSec" -lt "$warningDelta" ]; then
          key="WARNING"
          [ $ec -lt 1 ] && ec=1
        else
          key="OK"
        fi

        printf "[%s] last backup in repo %s from %s\n" "$key" "$repo" "$lastBackup"
      else
        # if lastBackup is empty, were having a encrypted repository and need to check the last backup age via the age of the nonce file
        if [ -n "$(find "${repo}/" -mindepth 0 -maxdepth 1 -name 'nonce' -type f -mmin +$((critical * 60)))" ]; then
          key="CRITICAL"
          ec=2
        elif [ -n "$(find "${repo}/" -mindepth 0 -maxdepth 1 -name 'nonce' -type f -mmin +$((warning * 60)))" ]; then
          key="WARNING"
          [ $ec -lt 1 ] && ec=1
        else
          key="OK"
        fi

        printf "[%s] repo %s is not readable, last modification of nonce file is %s\n" "$key" "$repo" "$(stat -c '%y' "${repo}/nonce" | cut -d. -f1)"
      fi
    fi
  done
fi

exit "$ec"
