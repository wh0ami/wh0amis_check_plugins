#!/bin/sh

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

response() {
  printf "BRATWURST STATUS "

  case "$2" in
    "0")
      printf "OK"
      ;;
    "1")
      printf "WARNING"
      ;;
    "2")
      printf "CRITICAL"
      ;;
    *)
      printf "UNKNOWN"
      ;;
  esac

  printf ": %s | " "$1"

  if [ -n "$3" ] && [ -n "$4" ]; then
    printf "request_success=1 feeder_up=%s mlat_up=%s" "$3" "$4"
  else
    printf "request_success=0"
  fi

  printf "\n"

  exit "$2"
}

CURL_PATH=$(which curl)
JQ_PATH=$(which jq)

if [ -z "$CURL_PATH" ]; then
  response "curl is not installed!" 3
fi
if [ -z "$JQ_PATH" ]; then
  response "jq is not installed!" 3
fi

TOKEN="$1"

if [ -z "$TOKEN" ]; then
  response "TOKEN was not specified!" 3
fi

RAW_DATA="$("$CURL_PATH" --fail --silent -L -H "Authorization: ${1}" https://api.bratwurst.network/feederOnline)"
CURL_EXIT_CODE=$?

if [ "$CURL_EXIT_CODE" -ne 0 ]; then
  response "Request to API failed, curl returned exit code '$CURL_EXIT_CODE'!" 2
fi

FEEDER_STATUS="$(echo "$RAW_DATA" | "$JQ_PATH" -r '.feederOnline.online')"
MLAT_STATUS="$(echo "$RAW_DATA" | "$JQ_PATH" -r '.feederOnline.mlatOnline')"

if [ "$FEEDER_STATUS" != "true" ] && [ "$MLAT_STATUS" != "true" ]; then
  response "Feeder and MLAT are DOWN!" 2 0 0
elif [ "$FEEDER_STATUS" != "true" ]; then
  response "Feeder is DOWN and MLAT is UP!" 1 0 1
elif [ "$MLAT_STATUS" != "true" ]; then
  response "Feeder is UP and MLAT is DOWN!" 1 1 0
else
  response "Feeder and MLAT are UP!" 0 1 1
fi
