#!/bin/bash

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

set -e

# check whether parameters are correctly passed and print help message otherwise
if [[ -z "$1" || -z "$2" || -z "$3" || "$1" == "--help" || "$1" == "-h" ]]; then
  printf "Usage: %s <username> <password> <hostname>\n" "$0"
  exit 3
fi

# determine whether lftp is installed - if not, abort
lftppath=$(which lftp)

if [[ -z "$lftppath" ]]; then
  printf "Please install lftp first!\n"
  exit 3
fi

# actually capture the data from the storagebox
readarray -t lftpoutput < <("$lftppath" -p 23 -u "$1","$2" "sftp://$3" -e "du -md1; bye" | grep -vE "^[0-9]+\s\.$")

printf "Collected usage data OK|"

for directory in "${lftpoutput[@]}"
do
  usage_mib=$(awk '{print $1}' <<< "$directory")
  path=$(basename "$(awk '{print $2}' <<< "$directory")")
  printf " %s=%sMiB" "$path" "$usage_mib"
done

printf "\n"
exit 0
