#!/bin/ash

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

apkpath=$(which apk)
sudopath=$(which sudo)

if [ -z "$apkpath" ] || [ -z "$sudopath" ]; then
  printf "Please install apk-tools and sudo first!\n"
  exit 3
fi

if [ "$1" = "-h" ]; then
  printf "Usage: %s %s\n" "$sudopath" "$0"
  exit 3
fi

updatesraw=$("$sudopath" "$apkpath" --no-cache upgrade -s|grep -vE "^fetch")
updateamount=$(printf "%s" "$updatesraw" | wc -l)
status=$(printf "%s" "$updatesraw" | tail -n1 | cut -d':' -f1)

if [ "$status" != "OK" ]; then
  printf "APK CRITICAL - integrity is %s\n" "$status"
  exitcode=2
elif [ "$updateamount" -gt 0 ]; then
  printf "APK WARNING - integrity is %s, %s update(s) available \n\n+++ Simulation +++\n%s | updateamount=%s\n" "$status" "$updateamount" "$(printf %s\\n "$updatesraw" | head -n -1)" "$updateamount"
  exitcode=1
elif [ "$updateamount" -eq 0 ]; then
  printf "APK OK - integrity is %s, no updates available | updateamount=%s\n" "$status" "$updateamount"
  exitcode=0
else
  printf "APK UNKNOWN - integrity is %s, raw output below...\n\n%s\n" "$status" "$updatesraw"
  exitcode=3
fi

exit $exitcode