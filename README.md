# wh0amis check plugins for Icinga2 (and Nagios)

This is my personal collection of monitoring plugins for my infrastructure, which is monitored via Icinga2.

The plugins are mostly written in Python3 or simple Shell script. Especially the Python3 based plugins are using the
libraries `nagiosplugin` and `psutil`, but there are also some custom implementations.

Plugins here are either not part of the default plugin bundle, or they provide a larger feature set than the given
plugin from the default bundle.

I do my best to keep all plugins compliant with
the [Nagios Developer Guidelines](https://nagios-plugins.org/doc/guidelines.html).

## Supported systems

Plugins here are basically written for Linux based systems. There is absolutely no support for Microsoft Windows
systems. Unix based systems are not my first priority, since I do not use unix, but I am open-minded for it and will try
to support it, if anybody needs it.

Due to the use of the `psutil` library, most Python3 plugins will require a Linux kernel greater equal version 3.2.0.

## Installation and updating

I personally prefer it, to place custom checks not in the default directory of icinga2. This is actually no problem for
icinga2 - you just have to keep in mind, that you need to provide the complete absolute path to the plugin in the
command definition. I am used to installing all custom check plugins in a folder called `/opt/icinga2-plugins/`.

I am mainly using Debian and Alpine Linux - so that's why I am providing a tutorial here. The plugins will work on many
other distributions and the way of installing them will be most likely very similar.

##### Debian Bookworm (12):

```
root@server:~# apt update && apt install python3 python3-dev python3-pip python3-requests python3-nagiosplugin python3-psutil python3-xmltodict python3-nut git sudo --no-install-recommends --no-install-suggests
[...]
root@server:~# apt purge --autoremove python3-nagiosplugin
[...]
root@server:~# cd /opt/icinga2_plugins/
root@server:/opt/icinga2_plugins# git clone https://codeberg.org/wh0ami/wh0amis_check_plugins
Cloning into 'wh0amis_check_plugins'...
remote: Enumerating objects: 799, done.
remote: Counting objects: 100% (799/799), done.
remote: Compressing objects: 100% (665/665), done.
remote: Total 799 (delta 524), reused 215 (delta 128), pack-reused 0
Receiving objects: 100% (799/799), 1.87 MiB | 4.10 MiB/s, done.
Resolving deltas: 100% (524/524), done.
root@server:/opt/icinga2_plugins#
```

On your own risk - if you want to use `check_nut2.py`:

```console
root@server:/opt/icinga2_plugins# pip3 install --break-system-packages nut2
root@server:/opt/icinga2_plugins#
```

On your own risk - if you want to use `check_tar1090.py`:

```console
root@server:/opt/icinga2_plugins# pip3 install --break-system-packages haversine
root@server:/opt/icinga2_plugins#
```

<br>

##### Alpine Linux (3):

```
root@server:~# apk update && apk add Linux-headers python3 python3-dev py3-pip git sudo
[...]
root@server:~# cd /opt/icinga2_plugins/
root@server:/opt/icinga2_plugins# git clone https://codeberg.org/wh0ami/wh0amis_check_plugins
Cloning into 'wh0amis_check_plugins'...
remote: Enumerating objects: 799, done.
remote: Counting objects: 100% (799/799), done.
remote: Compressing objects: 100% (665/665), done.
remote: Total 799 (delta 524), reused 215 (delta 128), pack-reused 0
Receiving objects: 100% (799/799), 1.87 MiB | 4.10 MiB/s, done.
Resolving deltas: 100% (524/524), done.
root@server:/opt/icinga2_plugins# cd wh0amis_check_plugins/
root@server:/opt/icinga2_plugins/wh0amis_check_plugins# pip3 install --upgrade -r python_checks/requirements.txt
[...]
root@server:/opt/icinga2_plugins/wh0amis_check_plugins# 
```

<br>

For updating, you simply have to `cd` into the plugin directory from above and do a `git pull`.

Of course, you can install the plugins wherever you want on your systems - that's only the way how I am doing it.

## Plugins and their features

| Plugin                                 | Checks...                                                                                                                               | Should work on...                                                                                                                                                       | Needs sudo grants                              |
|----------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| `check_borg_backups.sh`                | the age of the last backup of multiple BorgBackup repositories in a directory                                                           | all Linux systems with BorgBackup installed                                                                                                                             | depends on your filesystem permissions         | 
| `check_drive_state.sh`                 | whether hard drives are spinning or in idle mode                                                                                        | all Linux systems with spinning hard drives and hdparm + sudo installed                                                                                                 | yes (on `hdparm` binary)                       |
| `check_dropluks_activity.sh`           | the activity of a [DropLUKS](https://codeberg.org/wh0ami/dropluks) Docker container based on its logs                                   | all Linux systems running DropLUKS in docker + sudo installed                                                                                                           | yes (on `docker logs <containername>` command) |
| `check_cpu.py`                         | the usage of the cpu and provides the ability for detailed monitoring and warning                                                       | all Linux systems                                                                                                                                                       | no                                             |
| `check_diskio.py`                      | the workload of physical disks in MiB/s or IOPS                                                                                         | all Linux systems with persistent storage                                                                                                                               | no                                             |
| `check_jitsi.py`                       | the workload of a [Jitsi Meet](https://github.com/jitsi/jitsi-meet) instance                                                            | all Linux systems (make sure to [enable the statistics endpoint](https://forum.netcup.de/sonstiges/smalltalk/p176655-das-l%C3%A4ngste-thema/#post176655) of jitsi meet) | no                                             |
| `check_memory.py`                      | the usage of the system memory                                                                                                          | all Linux systems                                                                                                                                                       | no                                             |
| `check_networkio.py`                   | the workload of the network interfaces in Mbit/s or PPS, also shows error and drop rates                                                | all Linux systems with network interfaces                                                                                                                               | no                                             |
| `check_tar1090.py`                     | the reported airplanes, messages, etc. of a [tar1090](https://github.com/wiedehopf/tar1090) instance. Supports also PiAware SkyAware.   | all Linux systems (could be used on localhost as well as remotely)                                                                                                      | no                                             |
| `check_pihole_update.py`               | whether a local installation of [PiHole](https://pi-hole.net/) needs an update                                                          | all Linux systems with PiHole installed on it (natively)                                                                                                                | no                                             |
| `check_nextcloud_update.py`            | whether a remote installation of [Nextcloud](https://github.com/nextcloud/server) needs an update                                       | all Linux systems                                                                                                                                                       | no                                             |
| `check_procstatus.py`                  | the amount of processes of a given process type (e.g `zombie`, `running`, ...)                                                          | all Linux systems                                                                                                                                                       | no                                             |
| `check_proc_mem.py`                    | the Resident Set-Size of a process                                                                                                      | all linux systems                                                                                                                                                       | no                                             |
| `check_swap.py`                        | the usage of the swap space as well as the even more important swap I/O                                                                 | all Linux systems                                                                                                                                                       | no                                             |
| `check_uptime.py`                      | how long a system is up and whether it needs a reboot                                                                                   | all Linux systems, reboot check currently only works on Debian and Alpine Linux based systems                                                                           | no                                             |
| `check_nut.py`                         | the health parameters of an UPS that is connected via [NUT](https://networkupstools.org/)                                               | debian based Linux systems, tested with PowerWalker VI 650 SH (USB) and APC Smart-UPS 750 RM + AP9630 (SNMPv3)                                                          | no                                             |
| `check_apk_updates.sh`                 | the amount of uninstalled updates on an Alpine Linux system                                                                             | all Alpine Linux systems                                                                                                                                                | yes (on `apk --no-cache upgrade -s` command)   |                                        
| `check_hetzner_storagebox.py`          | the usage of a [Hetzner Storagebox](https://www.hetzner.com/storage/storage-box)                                                        | all Linux systems                                                                                                                                                       | no                                             |                                        
| `check_hetzner_storagebox_dirusage.sh` | the usage of the specific directories in a [Hetzner Storagebox](https://www.hetzner.com/storage/storage-box)                            | all Linux systems with bash and lftp installed                                                                                                                          | no                                             |                                        
| `check_ds18b20.py`                     | the sensor reading of a DS18B20 temperature sensor via 1-Wire                                                                           | all Linux systems                                                                                                                                                       | no                                             |
| `check_systemd_failed_units.sh`        | the amount of failed systemd units                                                                                                      | all Linux systems, which are using systemd                                                                                                                              | no                                             |
| `check_shellyplug-s_power.py`          | the power throughput of a ShellyPlug-S (Gen 1 API)                                                                                      | all Linux systems                                                                                                                                                       | no                                             |
| `check_shellyplusplug-s_power.py`      | the power throughput of a ShellyPlusPlug-S (Gen 2 API)                                                                                  | all Linux systems                                                                                                                                                       | no                                             |
| `check_bratwurst_feeder_status.sh`     | the connection status of an ADS-B feeder incl. its MLAT client to the server of the [Bratwurst project](https://info.bratwurst.network) | all Linux systems with curl and jq installed                                                                                                                            | no                                             |
| `check_onlyoffice_documentserver.sh`   | the Healthcheck endpoint of a OnlyOffice Documentserver                                                                                 | all Linux systems with curl installed                                                                                                                                   | no                                             |

If you want to see the full feature set of a plugin or if you need help, you can always pass the parameter `-h` to show
up a documentation of all parameters.

## Special docs for `check_hetzner_storagebox.py`

For this check plugin, you need to pass a username, a password and a Storagebox ID. Here's a quick guide, how you can
get them:

- Username and Password >> [Hetzner Robot](https://robot.your-server.de/)
    - You can set a password under `User Icon`/`Settings`/`Webservice and app Settings`
    - The username will be sent to you via email
- The identifier of your Storagebox >> do a quick curl
    - `curl -u '<user>:<password>' https://robot-ws.your-server.de/storagebox | jq '.'` (jq is optional)
    - the command will return a list with all of your Storageboxes - each Storagebox entry is having the necessary `id`
      attribute

## Special docs for `check_hetzner_storagebox_dirusage.sh`

The check has to be called in the following
way: `./check_hetzner_storagebox_dirusage.sh "u123456" "mysecretpassword" "u123456.your-storagebox.de"`

The credentials are those of a sub-user of the storagebox. In my specific case, I created a directory called `backups/`
and added a user with read only permissions on that directory. In this directory, I created subdirectories with
additional users for each host, that are having read write permissions in their specific directory.

I am using the mentioned read only user for that check here. In that way, it's possible to avoid, that my monitoring
server is able to reduce my storage bills.

If the check runs into a timeout, you may want to make sure, that the user of your Icinga2 agent has the SSH host keys
from the target storagebox in his cache.

## Special docs for `check_bratwurst_feeder_status.sh`

The check has to be called in the following
way: `./check_bratwurst_feeder_status.sh "mytoken"`

The token can be found in the file `/root/bratwurstUUID` on your feeder.

## Special docs for `check_onlyoffice_documentserver.sh`

The check has to be called in the following
way: `./check_onlyoffice_documentserver.sh "DNS Name" "Endpoint path"`

Example: `./check_onlyoffice_documentserver.sh "cloud.example.org" "/onlyoffice/healthcheck"`

## Problems, ideas, criticism, ...

Found a bug? Needing a feature? Having a good idea to improve the project? Or found anything annoying?

I would be happy to receive feedback, and I am also open-minded against co-workers in this project. You are welcome to
create issues and merge requests!

## Licensing

This project is using [the Unlicense](https://unlicense.org/). (see also `LICENSE` file)

## Credits / used libraries

Many thanks for the following projects and its maintainers - without them, this project couldn't exist!

- [Python3] **[nagiosplugin](https://pypi.org/project/nagiosplugin/)**
- [Python3] **[psutil](https://pypi.org/project/psutil/)**
- [Python3] **[haversine](https://pypi.org/project/haversine/)**
- [Python3] **[requests](https://pypi.org/project/requests/)**
- [Python3] **[nut2](https://pypi.org/project/nut2/)**
- [Python3] **[xmltodict](https://pypi.org/project/xmltodict/)**
- [Shell] **[bebehei's check_borg](https://github.com/bebehei/nagios-plugin-check_borg/blob/master/check_borg)**
- [IDE] **[Jetbrains PyCharm Community Edition](https://www.jetbrains.com/pycharm/)**
